$(function(){
	var spW = 767;
	var winW = $(window).width();
	var winH = $(window).height();

	$(window).resize(function(){
		winW = $(window).width();
		winH = $(window).height();
	});

	//画像のロールオーバー機能（オーバー画像別）
	if(spW < winW){
		jTools.rollOverImg.init("img.over" , "_over" , "0.6");
	}

	/*=========================================
	 * ページ遷移
	 *=======================================*/
	$("a[data-scroll]").on("click", function(e){
		e.preventDefault();
		var pos = $($(this).attr("href")).offset().top;
		$(/safari/i.test(navigator.userAgent) ? 'body' : 'html').stop().animate({ scrollTop:pos },{ duration: 600, easing: "swing" });
		return false;
	});

	/*=========================================
	 * 下部エリア移動処理
	 *=======================================*/
	$(".section-box a.scroll-btn").each(function(key , value){
		$(value).click(function(e){
			e.preventDefault();
			var pos = $(".section-box").eq((key+1)).offset().top;
			var H = $(".section-box").eq((key+1)).outerHeight();
			if(H < winH){
				pos = pos - Math.floor((winH - H) / 2);
			}
			$(/safari/i.test(navigator.userAgent) ? 'body' : 'html').stop().animate({ scrollTop:pos },{ duration: 600, easing: "swing" });
		});
	});

	/*=========================================
	 * 応募ページ
	 *=======================================*/
	$(".imp").each(function(key,value){
		$(this).append('<div class="asterisk"></div>');
	});

	/*入力チェック*/
	$("form.formToConfirm").submit(function(e){
		$(".form-error").remove();
		var bError = true

		/*必須形式1*/
		$(".required1").each(function(key,value){
			if($(value).val() == ""){
				$(value).after('<p class="form-error">必須項目です。</p>');
				bError = false;
			}
		});

		/*生年月日2*/
		var bError2 = false;
		var bError3 = false;
		$(".required2").each(function(key,value){
			var numeric = $(value).val();
			if(numeric == ""){
				bError = false;
				bError2 = true;
			}
			if (numeric != "" && !numeric.match(/^\d{1,2}$/)){
				bError = false;
				bError3 = true;
			}
		});
		if(bError2){
			$(".select-box2").parent().parent().append('<p class="form-error">必須項目です。</p>');
		}
		if(bError3){
			$(".select-box2").parent().parent().append('<p class="form-error">2桁以内の半角数値で入力してください。</p>');
		}


		//郵便番号チェック
		var postal = $(".txtPos").val();
		if (postal != "" && !postal.match(/^\d{3}-?\d{4}$/)){
			$(".txtPos").after('<p class="form-error">郵便番号の形式が正しくありません。</p>');
			bError = false;
		}

		//Emailチェック
		$(".mail").each(function(key,value){
			var mail = $(value).val();
			if (mail != "" && !mail.match(/^[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-]+(\.[a-zA-Z0-9_\-]+)+$/)){
				$(value).after('<p class="form-error">メールアドレスの形式が正しくありません。</p>');
				bError = false;
			}
		});

		//電話番号チェック
		$(".phone").each(function(key,value){
			var phone = $(value).val();
			if (phone != "" && !phone.match(/^[0-9\-]+[0-9\-]+[0-9]+$/)){
				$(value).after('<p class="form-error">番号の形式が正しくありません。</p>');
				bError = false;
			}
		});

		/*同意チェック*/
		$("input[type='checkbox']").each(function(key,value){
			if(!$(value).prop('checked')){
				$(value).parent().after('<p class="form-error">必須項目です。</p>');
				bError = false;
			}
		});

		return bError;
	});
});

$(function(){
    $("#pagetop").click(function(){
        $("html,body").animate({scrollTop:0},'slow');
        return false;
    });
});